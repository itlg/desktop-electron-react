import React from 'react';
import { Window, TitleBar, Text } from 'react-desktop/windows';

const HomeComponent = () => {
  return (
    <Window
      chrome
      height="300px"
      padding="12px"
    >
      <TitleBar title="My Windows Application" />
      <Text>Hello World</Text>
    </Window>
  );
};


export default HomeComponent;
