import * as grpc from '@grpc/grpc-js';
import {PackageDefinition} from "@grpc/proto-loader";

import * as protoDefinition from './stubs/itbridge_grpc_pb'
import {iTBridgeServiceClient} from './stubs/itbridge_grpc_pb'
import {Empty, HelloRequest, TrackLyrics, TracksListStream} from "./stubs/itbridge_pb";

export class GrpcService {
  private client: iTBridgeServiceClient;

  constructor() {
    const packageDefinition = grpc.loadPackageDefinition(protoDefinition as unknown as PackageDefinition)
    // @ts-ignore
    const iTBridgeServiceClient = packageDefinition.itbridge.iTBridgeService as unknown as iTBridgeServiceClient

    // @ts-ignore
    this.client = new iTBridgeServiceClient('localhost:50051', grpc.credentials.createInsecure(), {})

    // do call
    const request = new HelloRequest()
    request.setName("Hello world!")

    this.client.sayHello(request, (err, res) => {
      console.log("sayHello:res:" + res)
      console.log("sayHello:err:" + err)
    })

    // do stream
    const streamedTracks = this.client.tracksGetSelected(new Empty())
    streamedTracks.on('data', (t: TracksListStream) => console.log(`track::n=${t.getCurrent()} id=${t.getTrack().getIdSource()};${t.getTrack().getIdDatabase()};${t.getTrack().getIdTrack()};${t.getTrack().getIdPlaylist()} name=${t.getTrack().getTitle()} album::${t.getTrack().getAlbum()}`))
    streamedTracks.on('err', e => console.log(e))
    streamedTracks.on('status', s => console.log(s))
    streamedTracks.on('end', (_: any) => console.log("Stream End!"))

    // update track lyrics
    const requestS = new TrackLyrics()
    requestS.setIdSource(69)
    requestS.setIdDatabase(100)
    requestS.setIdPlaylist(268)
    requestS.setIdTrack(271)
    requestS.setLyrics("iTLG Works!")

    this.client.trackSetLyrics(requestS, (err, res) => {
      console.log("trackSetLyrics:res:" + res.getOk())
      console.log("trackSetLyrics:err:" + err)
    })
  }

  test(): void {
    console.log("hello")
  }
}
