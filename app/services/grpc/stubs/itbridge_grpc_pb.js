// GENERATED CODE -- DO NOT EDIT!

'use strict';
var itbridge_pb = require('./itbridge_pb.js');

function serialize_itbridge_Empty(arg) {
  if (!(arg instanceof itbridge_pb.Empty)) {
    throw new Error('Expected argument of type itbridge.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_Empty(buffer_arg) {
  return itbridge_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_HelloReply(arg) {
  if (!(arg instanceof itbridge_pb.HelloReply)) {
    throw new Error('Expected argument of type itbridge.HelloReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_HelloReply(buffer_arg) {
  return itbridge_pb.HelloReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_HelloRequest(arg) {
  if (!(arg instanceof itbridge_pb.HelloRequest)) {
    throw new Error('Expected argument of type itbridge.HelloRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_HelloRequest(buffer_arg) {
  return itbridge_pb.HelloRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_OperationResult(arg) {
  if (!(arg instanceof itbridge_pb.OperationResult)) {
    throw new Error('Expected argument of type itbridge.OperationResult');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_OperationResult(buffer_arg) {
  return itbridge_pb.OperationResult.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_TrackLyrics(arg) {
  if (!(arg instanceof itbridge_pb.TrackLyrics)) {
    throw new Error('Expected argument of type itbridge.TrackLyrics');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_TrackLyrics(buffer_arg) {
  return itbridge_pb.TrackLyrics.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_TracksListStream(arg) {
  if (!(arg instanceof itbridge_pb.TracksListStream)) {
    throw new Error('Expected argument of type itbridge.TracksListStream');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_TracksListStream(buffer_arg) {
  return itbridge_pb.TracksListStream.deserializeBinary(new Uint8Array(buffer_arg));
}


var iTBridgeServiceService = exports['itbridge.iTBridgeService'] = {
  sayHello: {
    path: '/itbridge.iTBridgeService/SayHello',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.HelloRequest,
    responseType: itbridge_pb.HelloReply,
    requestSerialize: serialize_itbridge_HelloRequest,
    requestDeserialize: deserialize_itbridge_HelloRequest,
    responseSerialize: serialize_itbridge_HelloReply,
    responseDeserialize: deserialize_itbridge_HelloReply,
  },
  tracksGetSelected: {
    path: '/itbridge.iTBridgeService/TracksGetSelected',
    requestStream: false,
    responseStream: true,
    requestType: itbridge_pb.Empty,
    responseType: itbridge_pb.TracksListStream,
    requestSerialize: serialize_itbridge_Empty,
    requestDeserialize: deserialize_itbridge_Empty,
    responseSerialize: serialize_itbridge_TracksListStream,
    responseDeserialize: deserialize_itbridge_TracksListStream,
  },
  trackSetLyrics: {
    path: '/itbridge.iTBridgeService/TrackSetLyrics',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.TrackLyrics,
    responseType: itbridge_pb.OperationResult,
    requestSerialize: serialize_itbridge_TrackLyrics,
    requestDeserialize: deserialize_itbridge_TrackLyrics,
    responseSerialize: serialize_itbridge_OperationResult,
    responseDeserialize: deserialize_itbridge_OperationResult,
  },
};

