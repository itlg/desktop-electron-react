// package: itbridge
// file: itbridge.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as itbridge_pb from "./itbridge_pb";

interface IiTBridgeServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    sayHello: IiTBridgeServiceService_ISayHello;
    tracksGetSelected: IiTBridgeServiceService_ITracksGetSelected;
    trackSetLyrics: IiTBridgeServiceService_ITrackSetLyrics;
}

interface IiTBridgeServiceService_ISayHello extends grpc.MethodDefinition<itbridge_pb.HelloRequest, itbridge_pb.HelloReply> {
    path: string; // "/itbridge.iTBridgeService/SayHello"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.HelloRequest>;
    requestDeserialize: grpc.deserialize<itbridge_pb.HelloRequest>;
    responseSerialize: grpc.serialize<itbridge_pb.HelloReply>;
    responseDeserialize: grpc.deserialize<itbridge_pb.HelloReply>;
}
interface IiTBridgeServiceService_ITracksGetSelected extends grpc.MethodDefinition<itbridge_pb.Empty, itbridge_pb.TracksListStream> {
    path: string; // "/itbridge.iTBridgeService/TracksGetSelected"
    requestStream: false;
    responseStream: true;
    requestSerialize: grpc.serialize<itbridge_pb.Empty>;
    requestDeserialize: grpc.deserialize<itbridge_pb.Empty>;
    responseSerialize: grpc.serialize<itbridge_pb.TracksListStream>;
    responseDeserialize: grpc.deserialize<itbridge_pb.TracksListStream>;
}
interface IiTBridgeServiceService_ITrackSetLyrics extends grpc.MethodDefinition<itbridge_pb.TrackLyrics, itbridge_pb.OperationResult> {
    path: string; // "/itbridge.iTBridgeService/TrackSetLyrics"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.TrackLyrics>;
    requestDeserialize: grpc.deserialize<itbridge_pb.TrackLyrics>;
    responseSerialize: grpc.serialize<itbridge_pb.OperationResult>;
    responseDeserialize: grpc.deserialize<itbridge_pb.OperationResult>;
}

export const iTBridgeServiceService: IiTBridgeServiceService;

export interface IiTBridgeServiceServer {
    sayHello: grpc.handleUnaryCall<itbridge_pb.HelloRequest, itbridge_pb.HelloReply>;
    tracksGetSelected: grpc.handleServerStreamingCall<itbridge_pb.Empty, itbridge_pb.TracksListStream>;
    trackSetLyrics: grpc.handleUnaryCall<itbridge_pb.TrackLyrics, itbridge_pb.OperationResult>;
}

export interface IiTBridgeServiceClient {
    sayHello(request: itbridge_pb.HelloRequest, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    sayHello(request: itbridge_pb.HelloRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    sayHello(request: itbridge_pb.HelloRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    tracksGetSelected(request: itbridge_pb.Empty, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    tracksGetSelected(request: itbridge_pb.Empty, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    trackSetLyrics(request: itbridge_pb.TrackLyrics, callback: (error: grpc.ServiceError | null, response: itbridge_pb.OperationResult) => void): grpc.ClientUnaryCall;
    trackSetLyrics(request: itbridge_pb.TrackLyrics, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.OperationResult) => void): grpc.ClientUnaryCall;
    trackSetLyrics(request: itbridge_pb.TrackLyrics, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.OperationResult) => void): grpc.ClientUnaryCall;
}

export class iTBridgeServiceClient extends grpc.Client implements IiTBridgeServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public sayHello(request: itbridge_pb.HelloRequest, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    public sayHello(request: itbridge_pb.HelloRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    public sayHello(request: itbridge_pb.HelloRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    public tracksGetSelected(request: itbridge_pb.Empty, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    public tracksGetSelected(request: itbridge_pb.Empty, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    public trackSetLyrics(request: itbridge_pb.TrackLyrics, callback: (error: grpc.ServiceError | null, response: itbridge_pb.OperationResult) => void): grpc.ClientUnaryCall;
    public trackSetLyrics(request: itbridge_pb.TrackLyrics, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.OperationResult) => void): grpc.ClientUnaryCall;
    public trackSetLyrics(request: itbridge_pb.TrackLyrics, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.OperationResult) => void): grpc.ClientUnaryCall;
}
