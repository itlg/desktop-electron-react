// package: itbridge
// file: itbridge.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class HelloRequest extends jspb.Message { 
    getName(): string;
    setName(value: string): HelloRequest;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HelloRequest.AsObject;
    static toObject(includeInstance: boolean, msg: HelloRequest): HelloRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HelloRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HelloRequest;
    static deserializeBinaryFromReader(message: HelloRequest, reader: jspb.BinaryReader): HelloRequest;
}

export namespace HelloRequest {
    export type AsObject = {
        name: string,
    }
}

export class HelloReply extends jspb.Message { 
    getMessage(): string;
    setMessage(value: string): HelloReply;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HelloReply.AsObject;
    static toObject(includeInstance: boolean, msg: HelloReply): HelloReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HelloReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HelloReply;
    static deserializeBinaryFromReader(message: HelloReply, reader: jspb.BinaryReader): HelloReply;
}

export namespace HelloReply {
    export type AsObject = {
        message: string,
    }
}

export class Empty extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Empty.AsObject;
    static toObject(includeInstance: boolean, msg: Empty): Empty.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Empty, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Empty;
    static deserializeBinaryFromReader(message: Empty, reader: jspb.BinaryReader): Empty;
}

export namespace Empty {
    export type AsObject = {
    }
}

export class OperationResult extends jspb.Message { 
    getOk(): boolean;
    setOk(value: boolean): OperationResult;

    getResultId(): number;
    setResultId(value: number): OperationResult;

    getMessage(): string;
    setMessage(value: string): OperationResult;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): OperationResult.AsObject;
    static toObject(includeInstance: boolean, msg: OperationResult): OperationResult.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: OperationResult, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): OperationResult;
    static deserializeBinaryFromReader(message: OperationResult, reader: jspb.BinaryReader): OperationResult;
}

export namespace OperationResult {
    export type AsObject = {
        ok: boolean,
        resultId: number,
        message: string,
    }
}

export class Track extends jspb.Message { 
    getIdSource(): number;
    setIdSource(value: number): Track;

    getIdPlaylist(): number;
    setIdPlaylist(value: number): Track;

    getIdTrack(): number;
    setIdTrack(value: number): Track;

    getIdDatabase(): number;
    setIdDatabase(value: number): Track;

    getTitle(): string;
    setTitle(value: string): Track;

    getArtist(): string;
    setArtist(value: string): Track;

    getAlbum(): string;
    setAlbum(value: string): Track;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Track.AsObject;
    static toObject(includeInstance: boolean, msg: Track): Track.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Track, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Track;
    static deserializeBinaryFromReader(message: Track, reader: jspb.BinaryReader): Track;
}

export namespace Track {
    export type AsObject = {
        idSource: number,
        idPlaylist: number,
        idTrack: number,
        idDatabase: number,
        title: string,
        artist: string,
        album: string,
    }
}

export class TracksList extends jspb.Message { 
    getQuantity(): number;
    setQuantity(value: number): TracksList;

    clearTracksList(): void;
    getTracksList(): Array<Track>;
    setTracksList(value: Array<Track>): TracksList;
    addTracks(value?: Track, index?: number): Track;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TracksList.AsObject;
    static toObject(includeInstance: boolean, msg: TracksList): TracksList.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TracksList, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TracksList;
    static deserializeBinaryFromReader(message: TracksList, reader: jspb.BinaryReader): TracksList;
}

export namespace TracksList {
    export type AsObject = {
        quantity: number,
        tracksList: Array<Track.AsObject>,
    }
}

export class TracksListStream extends jspb.Message { 
    getCurrent(): number;
    setCurrent(value: number): TracksListStream;

    getTotal(): number;
    setTotal(value: number): TracksListStream;


    hasTrack(): boolean;
    clearTrack(): void;
    getTrack(): Track | undefined;
    setTrack(value?: Track): TracksListStream;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TracksListStream.AsObject;
    static toObject(includeInstance: boolean, msg: TracksListStream): TracksListStream.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TracksListStream, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TracksListStream;
    static deserializeBinaryFromReader(message: TracksListStream, reader: jspb.BinaryReader): TracksListStream;
}

export namespace TracksListStream {
    export type AsObject = {
        current: number,
        total: number,
        track?: Track.AsObject,
    }
}

export class TrackLyrics extends jspb.Message { 
    getIdSource(): number;
    setIdSource(value: number): TrackLyrics;

    getIdPlaylist(): number;
    setIdPlaylist(value: number): TrackLyrics;

    getIdTrack(): number;
    setIdTrack(value: number): TrackLyrics;

    getIdDatabase(): number;
    setIdDatabase(value: number): TrackLyrics;

    getLyrics(): string;
    setLyrics(value: string): TrackLyrics;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TrackLyrics.AsObject;
    static toObject(includeInstance: boolean, msg: TrackLyrics): TrackLyrics.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TrackLyrics, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TrackLyrics;
    static deserializeBinaryFromReader(message: TrackLyrics, reader: jspb.BinaryReader): TrackLyrics;
}

export namespace TrackLyrics {
    export type AsObject = {
        idSource: number,
        idPlaylist: number,
        idTrack: number,
        idDatabase: number,
        lyrics: string,
    }
}
