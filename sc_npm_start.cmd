cd ../itbridge-win
devenv itbridge-win.sln /build Debug

cd ../desktop-electron-react/app
mkdir itbridge
cp -r ../../itbridge-win/bin/Debug/*.* ./itbridge
cd ..

yarn dev
